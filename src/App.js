import Entete from "./navbar/Entete";
import { Routes, Route } from "react-router-dom";
import Home from "./components/Home";
import AddProduct from "./components/product/AddProduct";
import ListProduct from "./components/product/ListProduct";
import { useState } from "react";
import AddUser from "./components/user/AddUser";
import ListUser from "./components/user/ListUser";
import SideBar from "./navbar/SideBar";
import Cart from "./components/Cart";
import "./styles/sidebar.css";

function App() {
  const [product, setProduct] = useState([]);
  const [user, setUser] = useState([]);

  const updateProduct = (item) => {
    setProduct([...product, item]);
  };

  const updateUser = (item) => {
    setUser([...user, item]);
  };

  return (
    <div className="main">
      <div className="sidebarApp">
        <SideBar />
      </div>
      <div className="container col-6">
        <Routes>
          <Route path="/">
            <Route index element={<Home />} />
            <Route path="Home" element={<Home />} />

            <Route
              path="AddProduct"
              element={<AddProduct updateProduct={updateProduct} />}
            />
            <Route
              path="ListProduct"
              element={<ListProduct product={product} />}
            />

            <Route
              path="AddUser"
              element={<AddUser updateUser={updateUser} />}
            />
            <Route path="ListUser" element={<ListUser user={user} />} />
            <Route path="Cart" element={<Cart />} />
          </Route>
        </Routes>
      </div>
    </div>
  );
}

export default App;
