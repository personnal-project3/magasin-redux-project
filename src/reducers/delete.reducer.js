import { DELETE_PRODUCT } from "../actions/delete.action";

const initialState = { products: [] };

export default function deleteReducer(state = initialState, action) {
  switch (action.type) {
    case DELETE_PRODUCT:
      const updateProduct = state.products.filter(
        (product) => product.id !== action.payload
      );
      return { ...state, products: updateProduct };
    default:
      return state;
  }
}
