import { combineReducers } from "redux";
import productReducer from "./cart.reducer";
import deleteReducer from "./delete.reducer";
export default combineReducers({
  productReducer,
  deleteReducer,
});
