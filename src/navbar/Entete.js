import React from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { Outlet, Link } from "react-router-dom";

const Entete = () => {
  return (
    <div>
      <div>
        <Navbar bg="light" expand="lg">
          <Container>
            <Navbar.Brand>
              <Link to="/Home">Navbar</Link>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <NavDropdown title="Produits" id="basic-nav-dropdown">
                  <NavDropdown.Item>
                    <Link to="/AddProduct"> Ajouter un Produit</Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <Link to="/ListProduct">Liste des produits</Link>
                  </NavDropdown.Item>
                </NavDropdown>
                <NavDropdown title="Utilisateur" id="basic-nav-dropdown">
                  <NavDropdown.Item>
                    <Link to="/AddUser">Ajouter un Utilisateur</Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <Link to="/ListUser">Liste des utilisateurs</Link>
                  </NavDropdown.Item>
                </NavDropdown>
                <Nav.Link href="#home">Panier</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        <Outlet />
      </div>
    </div>
  );
};

export default Entete;
