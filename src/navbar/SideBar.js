import React from "react";
import "../styles/sidebar.css";
import HomeIcon from "@mui/icons-material/Home";
import AppsOutlinedIcon from "@mui/icons-material/AppsOutlined";
import PeopleIcon from "@mui/icons-material/People";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { Link, Outlet } from "react-router-dom";
import Dropdown from "react-bootstrap/Dropdown";

const SideBar = () => {
  return (
    <>
      <div className="sidebar">
        <ul className="sidebarList">
          <h2>CROSS-BIKER</h2>
          <Link to="/Home" className="ligne">
            <div id="icons">
              <HomeIcon />
            </div>
            <div id="title">Accueil</div>
          </Link>
          <Dropdown className="ligne">
            <Dropdown.Toggle variant="light">
              <AppsOutlinedIcon />
              &nbsp;Produits
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <div>
                <Link to="/AddProduct">Ajouter un produit</Link>
              </div>
              <div>
                <Link to="/ListProduct">Liste des produits</Link>
              </div>
            </Dropdown.Menu>
          </Dropdown>
          <Dropdown className="ligne">
            <Dropdown.Toggle variant="light" className="">
              <PeopleIcon />
              &nbsp;Utilisateurs
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <div>
                <Link to="/AddUser">Ajouter un Utilisateur</Link>
              </div>
              <div>
                <Link to="/ListUser">Liste des Utilisateurs</Link>
              </div>
            </Dropdown.Menu>
          </Dropdown>
          <Link to="/Cart" className="ligne">
            <div id="icons">
              <ShoppingCartIcon />
            </div>
            <div id="title">Panier</div>
          </Link>
        </ul>
        <Outlet />
      </div>
    </>
  );
};

export default SideBar;
