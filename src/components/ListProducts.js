import kawasaki from "../assets/KAWASAKI-23MY_Z900_GY1_STU.png";
import ducati from "../assets/Ducati-panigale-v4-720625.jpg";
import triumph from "../assets/TRIUMPH.jpg";
import yamaha from "../assets/2023-Yamaha-YZ250F.jpg";

export const ListProducts = [
  {
    id: "hdjd",
    name: "KAWASAKI-23MY",
    price: "7 000 000",
    quantity: 2,
    image: kawasaki,
  },
  {
    id: "1dxx",
    name: "DUCATI PANIGALE",
    price: "1 800 000",
    quantity: 8,
    image: ducati,
  },
  {
    id: "5xsz",
    name: "TRIUMPH",
    price: "4 000 000",
    quantity: 5,
    image: triumph,
  },
  {
    id: "5xsz",
    name: "YAMAHA YZ2",
    price: "600 000",
    quantity: 5,
    image: yamaha,
  },
];

export default ListProducts;
