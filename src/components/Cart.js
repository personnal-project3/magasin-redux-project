import React from "react";
import { isEmpty } from "./Utils";
import { useSelector } from "react-redux";
import "../styles/cart.css";
import {
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCol,
  MDBContainer,
  MDBRow,
} from "mdb-react-ui-kit";

const Cart = () => {
  const CartItems = useSelector((state) => state.productReducer.cart);

  return (
    <div className="">
      <h2>VOTRE PANIER</h2>
      {!isEmpty(CartItems) &&
        CartItems.map((item, index) => {
          return (
            <section
              className="w-100  "
              style={{ backgroundColor: "" }}
              key={index}
            >
              <MDBContainer className="p-4">
                <MDBCol>
                  <MDBCard className="">
                    <MDBCardBody className="p-4">
                      <MDBRow className="align-items-center">
                        <MDBCol md="2">
                          <MDBCardImage fluid src={item.image} alt="logo" />
                        </MDBCol>
                        <MDBCol
                          md="2"
                          className="d-flex justify-content-center"
                        >
                          <div>
                            <p className="small text-muted mb-4 pb-2">Name</p>
                            <p className="lead fw-normal mb-0">{item.name}</p>
                          </div>
                        </MDBCol>
                        <MDBCol
                          md="2"
                          className="d-flex justify-content-center"
                        >
                          <div>
                            <p className="small text-muted mb-4 pb-2">
                              Specification du client
                            </p>
                            <p className="lead fw-normal mb-0">
                              {item.description}
                            </p>
                          </div>
                        </MDBCol>
                        <MDBCol
                          md="2"
                          className="d-flex justify-content-center"
                        >
                          <div>
                            <p className="small text-muted mb-4 pb-2">
                              Quantity
                            </p>
                            <p className="lead fw-normal mb-0">1</p>
                          </div>
                        </MDBCol>
                        <MDBCol
                          md="2"
                          className="d-flex justify-content-center"
                        >
                          <div>
                            <p className="small text-muted mb-4 pb-2">Price</p>
                            <p className="lead fw-normal mb-0">
                              {item.price} FCFA
                            </p>
                          </div>
                        </MDBCol>
                        <MDBCol
                          md="2"
                          className="d-flex justify-content-center"
                        >
                          <div>
                            <p className="small text-muted mb-4 pb-2">Total</p>
                            <p className="lead fw-normal mb-0">{item.price}</p>
                          </div>
                        </MDBCol>
                      </MDBRow>
                    </MDBCardBody>
                  </MDBCard>

                  <MDBCard className="mb-5">
                    <MDBCardBody className="p-4">
                      <div className="float-end">
                        <p className="mb-0 me-5 d-flex align-items-center">
                          <span className="small text-muted me-2">
                            Order total:
                          </span>
                          <span className="lead fw-normal">
                            {item.price} FCFA
                          </span>
                        </p>
                      </div>
                    </MDBCardBody>
                  </MDBCard>
                </MDBCol>
              </MDBContainer>
            </section>
          );
        })}
    </div>
  );
};

export default Cart;
