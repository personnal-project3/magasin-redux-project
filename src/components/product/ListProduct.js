import React from "react";
import { Card, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { addToCart } from "../../actions/cart.action";
import { deleteProduct } from "../../actions/delete.action";

const ListProduct = ({ product }) => {
  const dispatch = useDispatch();
  const handleAddToCart = (item) => {
    dispatch(addToCart(item));
  };
  const handleDeleteProduct = (productId) => {
    dispatch(deleteProduct(productId));
  };

  return (
    <>
      <div className="row">
        {product.map((item, index) => (
          <div className="col-md-3" key={index}>
            <Card>
              <Card.Img
                variant="top"
                src={item.image}
                className="card-img-top h-50"
              />
              <Card.Body>
                <Card.Title>{item.name}</Card.Title>
                <Card.Text>
                  Prix: {item.description} FCFA | Quantity: {item.price} FCFA
                </Card.Text>
                <Button variant="success" onClick={() => handleAddToCart(item)}>
                  Ajouter au Panier
                </Button>
                <Button
                  variant="danger"
                  onClick={() => handleDeleteProduct(item.index)}
                >
                  Supprimer
                </Button>
              </Card.Body>
            </Card>
          </div>
        ))}
      </div>
    </>
  );
};

export default ListProduct;
