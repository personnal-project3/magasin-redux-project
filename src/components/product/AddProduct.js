import React, { useState } from "react";

const AddProduct = ({ updateProduct }) => {
  const [validated, setValidate] = useState(false);
  const [product, setProduct] = useState({
    name: "",
    description: "",
    price: "",
    image: "",
  });

  const handleChange = (e) => {
    setProduct({ ...product, [e.target.name]: e.target.value });
  };

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    setProduct({ ...product, image: URL.createObjectURL(file) });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const formulaire = e.currentTarget;
    if (
      formulaire.checkValidity() === false ||
      product.price <= 1 ||
      product.image === ""
    ) {
      e.stopPropagation();
      alert("Veuillez remplir tout les champs");
    } else {
      updateProduct(product);
      setValidate(true);
      setProduct({
        name: "",
        description: "",
        price: "",
        image: "",
      });
      alert("Produit ajouté , veuillez consultez la liste ");
    }
  };

  return (
    <>
      <form
        className="container mt-3 m-5"
        noValidate
        //validated={validated}//
        onSubmit={handleSubmit}
      >
        <div className="row">
          <div className="col-md-6">
            <div className="mb-3">
              <label htmlFor="nom" className="form-label">
                Nom du Produit
              </label>
              <input
                type="text"
                className="form-control"
                id="nom"
                name="name"
                value={product.name}
                onChange={handleChange}
              />
            </div>
          </div>
          <div className="col-md-6">
            <div className="mb-3">
              <label htmlFor="infos" className="form-label">
                libellé
              </label>
              <input
                type="text"
                className="form-control"
                id="infos"
                name="description"
                value={product.description}
                onChange={handleChange}
              />
            </div>
          </div>
        </div>

        <div className="mb-3">
          <label htmlFor="prix" className="form-label">
            Prix
          </label>
          <input
            type="number"
            className="form-control"
            id="prix"
            name="price"
            value={product.price}
            onChange={handleChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="image" className="form-label">
            Image
          </label>
          <input
            type="file"
            className="form-control"
            id="image"
            name="image"
            onChange={handleImageChange}
          />
        </div>

        <div className="mb-3">
          <button type="submit" className="btn btn-success">
            Submit
          </button>
        </div>
      </form>
    </>
  );
};

export default AddProduct;
