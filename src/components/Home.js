import React from "react";
import ListProducts from "./ListProducts";
import { Card, Button } from "react-bootstrap";
import "../styles/home.css";
import { useDispatch } from "react-redux";
import { addToCart } from "../actions/cart.action";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";

const Home = () => {
  const dispatch = useDispatch();
  const handleAddToCart = (product) => {
    dispatch(addToCart(product));
  };

  return (
    <div>
      <h1 className="slogan">Bienvenue au paradis des motocyclistes!</h1>
      <div className="row p-5">
        {ListProducts.map((product) => (
          <div className="col-md-3" key={product.id}>
            <Card>
              <Card.Img variant="top" src={product.image} />
              <Card.Body>
                <Card.Title>{product.name}</Card.Title>
                <Card.Text>
                  Prix: {product.price} FCFA | Quantity: {product.quantity}
                </Card.Text>
                <Button
                  variant="success"
                  onClick={() => handleAddToCart(product)}
                >
                  Ajouter au panier <ShoppingCartIcon />
                </Button>
              </Card.Body>
            </Card>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Home;
