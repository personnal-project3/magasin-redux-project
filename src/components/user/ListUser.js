import React from "react";

import { Container, Card, Col, Row } from "react-bootstrap";

const ListUser = ({ user }) => {
  return (
    <div className="d-flex flex-grid">
      {user.map((item, index) => {
        return (
          <Container className="">
            <Col className="">
              <Card className="text-center">
                <ul
                  key={index}
                  className="list-group list-group-unbordered mb-3"
                >
                  <li className="list-group-item ">
                    <b>Identifiant :</b> <a> {item.id} </a>
                  </li>
                  <li className="list-group-item">
                    <b>Nom et prenom :</b> {item.fullname}
                  </li>
                  <li className="list-group-item">
                    <b>Télephone :</b> {item.phone}
                  </li>
                  <li className="list-group-item">
                    <b>Email :</b>
                    {item.email}
                  </li>
                  <li className="list-group-item">
                    <b>Adresse :</b> {item.address}
                  </li>
                </ul>
              </Card>
            </Col>
          </Container>
        );
      })}
    </div>
  );
};

export default ListUser;
