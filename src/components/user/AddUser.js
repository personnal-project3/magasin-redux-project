import React, { useState } from "react";

const AddUser = ({ updateUser }) => {
  const [validated, setValidate] = useState(false);
  const [user, setUser] = useState({
    id: "",
    fullname: "",
    phone: "",
    email: "",
    adress: "",
  });

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const formulaire = e.currentTarget;
    if (formulaire.checkValidity() === false || user.phone < 1) {
      e.stopPropagation();
      alert("Veuillez remplir tout les champs");
    } else {
      updateUser(user);
      setValidate(true);
      setUser({
        id: "",
        fullname: "",
        phone: "",
        email: "",
        adress: "",
      });
      alert("Utilisateur enregistré");
    }
  };

  return (
    <>
      <form
        className="container mt-3 m-5"
        noValidate
        validated={validated}
        onSubmit={handleSubmit}
      >
        <div className="row">
          <div className="col-md-6">
            <div className="mb-3">
              <label htmlFor="id" className="form-label">
                Identifiant client :
              </label>
              <input
                type="text"
                className="form-control"
                id="id"
                name="id"
                value={user.id}
                onChange={handleChange}
              />
            </div>
          </div>
          <div className="col-md-6">
            <div className="mb-3">
              <label htmlFor="fullname" className="form-label">
                Nom et prenom :
              </label>
              <input
                type="text"
                className="form-control"
                id="fullname"
                name="fullname"
                value={user.fullname}
                onChange={handleChange}
              />
            </div>
          </div>
        </div>

        <div className="mb-3">
          <label htmlFor="phone" className="form-label">
            Téléphone :
          </label>
          <input
            type="number"
            className="form-control"
            id="phone"
            name="phone"
            value={user.phone}
            onChange={handleChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Adresse Email :
          </label>
          <input
            placeholder="monadresse@email.com"
            type="email"
            className="form-control"
            id="email"
            name="email"
            onChange={handleChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            Adresse Domicile :
          </label>
          <input
            type="texte"
            className="form-control"
            id="address"
            name="address"
            onChange={handleChange}
          />
        </div>

        <div className="mb-3">
          <button type="submit" className="btn btn-success">
            Enregistré
          </button>
        </div>
      </form>
    </>
  );
};

export default AddUser;
